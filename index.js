const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');
const app = express();

app.use(cors());

app.get('/people',
  findPeople
);

app.get('/planets',
  findPlanets,
);

async function findPeople(req, res, next) {
  let data = [];
  let nextPage = 'https://swapi.dev/api/people/';

  while (nextPage) {
    try {
      const response = await (await fetch(nextPage)).json();
      nextPage = response.next;
      data = data.concat(response.results);
    } catch (err) {
      console.error(err);
      return res.json({
        success: false,
        error: 'Something went wrong on the server.',
      });
    }
  }

  if (req.query.sortBy === 'name') {
    data.sort((a, b) => (a[req.query.sortBy] > b[req.query.sortBy]) ? 1 : -1);
  } else if (req.query.sortBy === 'height' || req.query.sortBy === 'mass') {
    data.sort((a, b) => {
      if (a[req.query.sortBy] === 'unknown') {
        return 1;
      } else if (b[req.query.sortBy] === 'unknown') {
        return -1;
      }
      return parseInt(a[req.query.sortBy].split(',').join('')) > parseInt(b[req.query.sortBy].split(',').join('')) ? 1 : -1;
    });
  }

  return res.json({
    success: true,
    response: data,
  });
}

async function findPlanets(req, res, next) {
  let data = [];
  let nextPage = 'https://swapi.dev/api/planets/';

  while (nextPage) {
    try {
      const response = await (await fetch(nextPage)).json();
      nextPage = response.next;
      data = data.concat(response.results);
    } catch (err) {
      console.error(err);
      return res.json({
        success: false,
        error: 'Something went wrong on the server.',
      });
    }
  }

  let promiseArray = [];
  for (let i = 0; i < data.length; i++) {
    if (data[i].residents) {
      for (let k = 0; k < data[i].residents.length; k++) {
        const charName = getCharacterName(data[i].residents[k]);
        if (charName.success === false) {
          return res.json(charName);
        } else {
          promiseArray.push(charName);
        }
      }
    }
  }

  Promise.all(promiseArray).then((values) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].residents) {
        for (let k = 0; k < data[i].residents.length; k++) {
          data[i].residents[k] = values.shift();
        }
      }
    }

    res.json({
      success: true,
      response: data,
    });
  })
  .catch ((err) =>{
    console.error(err);
    return res.json({
      success: false,
      error: 'Something went wrong on the server.',
    });
  });
}

async function getCharacterName(url) {
  try {
    const response = await (await fetch(url)).json();
    return response.name;
  } catch (err) {
    return {
      error: err,
      success: false
    };
  }
}

const server = app.listen(3000, () => {
  const host = server.address().address;
  const port = server.address().port;

  console.log('Listening at http://%s:%s', host, port);
});

